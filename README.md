# Localise.biz language file downloader

Downloads language files from [localise.biz](https://localise.biz)

## Installation

```
yarn add localise-dot-biz --dev
```

or
```
npm i localise-dot-biz --save-dev
```

## Configuration
Add the following to your package.json
```json
{
  "scripts": {
    "localize": "localise-dot-biz"
  },
  "i18n": {
    "outDir": "src/locales",
    "apiKeyEnvVar": "LOCALISE_BIZ_API_KEY",
    "format": "i18next3",
    "generateTSIndex": true,
    "generateTSEnum": true,
    "languages": [
      "en-US"
    ]
  }
}
```

### Options
| Property | Description | Required | Example |
| :--- | :--- | :---: | :--- |
| outDir   | The directory that the files will be created in | true | src/i18n |
| apiKeyEnvVar   | The environment variable that you have set your api key to | false | LOCALISE_BIZ_API_KEY |
| secretConf   | The directory that the files will be created in | false | localise-key.json |
| format | The localise.biz file format. `i18next3` is designed to work with the [i18n](https://www.npmjs.com/package/i18n) package | true | i18next3 |
| generateTSIndex | Whether or not to generate an index.ts file in the `outDir`. This is only needed if you are shipping a typescript package. If you don't have this option set your language files will not bin included in the output directory when you compile your typescript files with `tsc` | false | true |
| generateTSEnum | Generate a tyepscript enum for the keys of the language file mappings | false | true |
| languages | An array of languages that your application supports. These must be already configured in localise.biz | true | \[ "en-US" ] |

#### apiKeyEnvVar
Don't forget to set your API key as an environment variable
```bash
export LOCALISE_BIZ_API_KEY=your_api_key_goes_here
```

#### secretConf format
If you use a config file to store you api key rather than an environment variable the file should have a property `key` with your api key
```json
{ 
    "key": "your_api_key_goes_here"
}
```
If you use this approach you *should* not commit your api key to your version control repository

## Run

```bash
yarn localize
```

or

```bash
npm run localize
```

## Suggestions
It may be a good idea to combine localize with a pre-commit hook order to prevent having outdated language files

## License
MIT

## Contributors
- Josh Woodcock