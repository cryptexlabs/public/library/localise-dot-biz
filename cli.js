#!/usr/bin/env node

'use strict';

/*eslint-disable no-console*/
const {dirname} = require('path');
const rootDir = dirname(dirname(__dirname))
const packageJson = require(`${rootDir}/package.json`);
const https = require('https');
const fs = require('fs');

const languages = packageJson.i18n.languages;

const download = async () => {
    let apiKey;
    if(packageJson.i18n.secretConf) {
        const apiKeyConfig = require(`${rootDir}/${packageJson.i18n.secretConf}`);
        apiKey = apiKeyConfig.key;
    }else{
        apiKey = process.env[packageJson.i18n.apiKeyEnvVar]
    }
    const promises = [];
    const outDir = `${rootDir}/${packageJson.i18n.outDir}`
    if(!fs.existsSync(outDir)) {
        fs.mkdirSync(outDir);
    }
    for(const lang of languages){
        const file = fs.createWriteStream(`${rootDir}/${packageJson.i18n.outDir}/${lang}.json`);
        promises.push(new Promise((resolve, reject) => {
            https.get(`https://localise.biz/api/export/locale/${lang}.json?format=${packageJson.format}&key=${apiKey}`, function(response) {
                response.pipe(file);
                resolve();
            });
        }));
    }
    await Promise.all(promises);
}

const createTSIndex = async () => {
    if(packageJson.i18n.generateTSIndex) {
        const langVars = [];
        const file = fs.createWriteStream(`${rootDir}/${packageJson.i18n.outDir}/index.ts`);

        for(const lang of languages){
            const langVar = lang.replace(/-/g, '_');
            langVars.push(langVar)
            file.write(`import * as ${langVar} from "./${lang}.json";\n`)
        }
        file.write('\n')
        file.write(`export { ${ langVars.join(', ') } };\n`)
        file.end();
    }
}

const createTSEnum = async () => {
    if(packageJson.i18n.generateTSEnum && languages.length > 0) {
        const file = fs.createWriteStream(`${rootDir}/${packageJson.i18n.outDir}/enum.ts`);
        const lang1Name = languages[0];
        const langJSON = require(`${rootDir}/${packageJson.i18n.outDir}/${lang1Name}.json`);
        const keys = Object.keys(langJSON);

        file.write(`export enum LocalesEnum {\n`)
        for(const key of keys){
            const enumName = key.replace(/-/g, '_').toUpperCase();
            file.write(`  ${enumName} `)
            file.write(`= "${key}",\n`)
        }
        file.write(`}\n`)
        file.end();
    }
}

const run = async () => {
    await download();

    const promises = []
    promises.push(createTSIndex());
    promises.push(createTSEnum());
    await Promise.all(promises);
}

console.log('Started downloading language files from localize.biz')

run().then(() => {
    console.log('Finished downloading language files from localize.biz')
});